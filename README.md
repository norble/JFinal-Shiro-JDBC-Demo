#JFinal-Shiro-JDBC-Demo
简单实现JFinal与Shiro整合例子
1、工程通过Eclipse直接导入，部署到tomcat中；<br /> 
2、新建jfinal_shiro数据库，执行jfinal_shiro.sql;<br /> 
3、修改配置文件中数据库用户名和密码<br /> 
	~/jfinal_shiro/resource/jfinal.properties<br /> 
	~/jfinal_shiro/resource/shiro.ini<br /> 
4、运行。	<br /> 

例子中有3个用户xiaoming、xiaohong、xiaohuang密码分别是用户名（木有进行加密存储）<br /> 

roles：admin和user<br /> 
permissions：addUser、showUser、editUser、deleteUser<br /> 


使用到的插件：<br /> 
jfinalshiroplugin：在JFinal可以采用shiro注释<br /> 
	源码 http://git.oschina.net/myaniu/jfinalshiroplugin<br /> 
	使用 http://my.oschina.net/myaniu/blog/137205<br /> 


Beetl 模板引擎<br /> 
	http://www.oschina.net/p/beetl<br /> 
	页面中使用Shiro http://my.oschina.net/u/567839/blog/143109<br /> 